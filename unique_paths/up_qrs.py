import random
import math
import statistics
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.colors import LogNorm
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import json

#TODO: format for time on x-axis (epoch millis to something readable) 


def unique_paths(es, src, dest, tfrom, tto, ipv6, metric): 
    
    if (metric == 'pl'): 
        metric_q = "packet_loss"
        m_index = 'ps_packetloss'
        label = 'Packet Loss'

    elif (metric == 'owd'):
        metric_q = "abs_delay"
        m_index = 'ps_owd'
        label = 'One Way Delay'

    elif (metric == 'tp'):
        metric_q = "throughput"
        m_index = 'ps_throughput'
        label = 'Throughput'

    elif (metric == 'rt'):
        metric_q = "retransmits"
        m_index = 'ps_retransmits'
        label = 'Retransmits'

    else:
        print("nope")
        return False 

    queryh = {
        "size": 0,
        "query": {
          "bool": {
            "filter": [
              {
                "term": {
                  "src_host": {
                    "value": src
                  }
                }
              },
              {
                "term": {
                  "dest_host": {
                    "value": dest
                  }
                }
              },
              {
                "range": {
                  "timestamp": {
                    "gte": tfrom,
                    "lte": tto
                  }
                }
              },
              {
                "term": {
                  "destination_reached": {
                    "value": True
                  }
                }
              },
              {
                "term": {
                  "path_complete": {
                    "value": True
                  }
                }
              },
              {
                "term": {
                  "ipv6": {
                    "value": ipv6
                  }
                }
              }
            ]
          }
        },
          "aggs": {
            "the_hash": {
              "terms": {
                "field": "route-sha1",
                "order": {"_count" : "desc"},
                "size": 500
              }
            }
          }
        }
    
    datah = es.search(index='ps_trace', body=queryh)  
    hash_arr = []
    if not datah['aggregations']['the_hash']['buckets']:
        #print("no existing path for ", src, " and ", dest)
        return False
    for x in datah['aggregations']['the_hash']['buckets']:
        hash_arr.append(x['key'])
    
    #return(len(hash_arr))
    queryt = {
        "size": 10000,
        "sort": [
            {
              "timestamp": {
                "order": "asc"
              }
            }
        ], 
        "query": {
          "bool": {
            "filter": [
              {
                "term": {
                  "src_host": {
                    "value": src
                  }
                }
              },
              {
                "term": {
                  "dest_host": {
                    "value": dest
                  }
                }
              },
              {
                "range": {
                  "timestamp": {
                    "gte": tfrom,
                    "lte": tto
                  }
                }
              },
              {
                "term": {
                  "destination_reached": {
                    "value": True
                  }
                }
              },
              {
                "term": {
                  "path_complete": {
                    "value": True
                  }
                }
              },
              {
                "term": {
                  "ipv6": {
                    "value": ipv6
                  }
                }
              }
            ]
          }
        } 
    }
    
    raw_ts = scan(client=es, index="ps_trace", query=queryt, _source=["timestamp", "route-sha1"])
    
    querym = {
        "size": 10000,
        "sort": [
            {
              "timestamp": {
                "order": "asc"
              }
            }
        ], 
        "query": {
          "bool": {
            "filter": [
              {
                "term": {
                  "src_host": {
                    "value": src
                  }
                }
              },
              {
                "term": {
                  "dest_host": {
                    "value": dest
                  }
                }
              },
              {
                "range": {
                  "timestamp": {
                    "gte": tfrom,
                    "lte": tto
                  }
                }
              },
              {
                "term": {
                  "ipv6": {
                    "value": ipv6
                  }
                }
              }
            ]
          }
        } 
    }
    
    
    
    
    raw_xy = scan(client=es, index=m_index, query=querym, _source=["timestamp", metric_q])
    hash_ts = []
    metric_arr = []
    x_time_arr = []

    for item in raw_ts:
        hash_ts.append(item['_source'])
       
        
    for item in raw_xy:
        metric_arr.append(item['_source'][metric_q])
        x_time_arr.append(item['_source']['timestamp'])
        
    
    for item in hash_ts:
        done = False
        if (item != hash_ts[-1]):
            while (done != True):
                if (item == hash_ts[-1]):
                    done = True
                elif (item["route-sha1"] == hash_ts[(hash_ts.index(item))+1]["route-sha1"]):
                    del (hash_ts[(hash_ts.index(item))+1])
                else:
                    done = True
    #hash_arr is list of hashes
    #hash_ts is list of dicts {"route-sha1", "timestamp"} with no consecutive hashes the same
    #ts_metric is list of dicts {"timestamp", metric_q} which will be used for x and y axes
    #paths is a list of hops in the same order as the hashes in hash arr

    paths = [] 

  #colors = ['#ff4d4d', '#f3ff4d', '#4dff94', '#4dacff', '#ac4dff', '#ff7c4d', '#ff4dc4', '#64ff4d', '#ffdb4d', '#4dfff3', '#ffc0cb', '#bd9d88', '#ee82ee']
  #n = 0 

    for x in hash_arr:
        queryp = {
          "size": 0,
          "query": {
            "bool": {
              "filter": [
                {
                  "term": {
                    "route-sha1": {
                      "value": x
                    }
                  }
                },
                {
                  "term": {
                    "src_host": {
                      "value": src
                    }
                  }
                },
                {
                  "term": {
                    "dest_host": {
                      "value": dest
                    }
                  }
                },
                {
                  "term": {
                    "destination_reached": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "hops_list": {
                  "top_hits": {
                    "size": 1
                  }
                }
              }
            }
        rawp = es.search(index='ps_trace', body=queryp) 
        hops = rawp['aggregations']['hops_list']['hits']['hits'][0]['_source']['hops']
        paths.append(hops)
        #if (n >= 13): #TODO: change this if more colors are added
        #  colors.append('#ffffff')

        #n += 1
    x_real = []
    for x in x_time_arr:
        print(pd.to_datetime(x, unit='ms'))
        x_real.append(pd.to_datetime(x, unit='ms'))
    
    ax = plt.plot(x_real, metric_arr)
    plt.xlabel('Time')
    plt.ylabel(label)
  #TODO: time format
    htnp = np.zeros((len(hash_ts), 2))
    cnt = 0

    for item in hash_ts:
        htnp[cnt][0] = int(item["route-sha1"], 16)
        htnp[cnt][1] = item["timestamp"]
        cnt += 1
    
    ax.pcolormesh(htnp) 
    
    #TODO: I think I actually didn't need to bother with the colors thing. CHECK THIS WORKS!

    plt.show()
        
    return paths 

def up_test(es, tfrom, tto, ipv6, run):
    #To use this function, uncomment line 77, and comment out the rest of unique_paths after that
    querys = {
       "size": 0,
         "query": {
           "bool": {
             "filter": [
               {
                 "range": {
                   "timestamp": {
                     "lte": tto,
                      "gte": tfrom
                   }
                 }
               },
               {
                 "term": {
                   "destination_reached": {
                     "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "srcs": {
                  "terms": {
                    "field": "src_host",
                    "size": 500
                  }
                }
              }
            }
    datas = es.search(index='ps_trace', body=querys)
    src_arr = []
    for x in datas['aggregations']['srcs']['buckets']:
        src_arr.append(x['key'])
        
    queryd = {
       "size": 0,
         "query": {
           "bool": {
             "filter": [
               {
                 "range": {
                   "timestamp": {
                     "lte": tto,
                      "gte": tfrom
                   }
                 }
               },
               {
                 "term": {
                   "destination_reached": {
                     "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "dests": {
                  "terms": {
                    "field": "dest_host",
                    "size": 500
                  }
                }
              }
            }
    datad = es.search(index='ps_trace', body=queryd)
    dest_arr = []
    for x in datad['aggregations']['dests']['buckets']:
        dest_arr.append(x['key'])
    
    n = run
    arr = []
    while (n != 0):
        thing = unique_paths(es, random.choice(src_arr), random.choice(dest_arr), tfrom, tto, ipv6)
        if (thing != False):
            arr.append(thing)
            print(n)
            n -= 1
        
    sd = statistics.stdev(arr)
    avg = statistics.mean(arr)
    
    print("Average number of paths per s/d pair: ", avg)
    print("Standard deviation of paths per s/d pair: ", sd)