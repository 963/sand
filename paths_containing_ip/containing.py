import random
import math
import statistics

def unique_paths(es, src, dest, tfrom, tto, ipv6): #TODO: add metric somewhere when relevant
    queryh = {
      "size": 0,
      "query": {
        "bool": {
          "filter": [
            {
              "term": {
                "src_host": {
                  "value": src
                }
              }
            },
            {
              "term": {
                "dest_host": {
                  "value": dest
                }
              }
            },
            {
              "range": {
                "timestamp": {
                  "gte": tfrom,
                  "lte": tto
                }
              }
            },
            {
              "term": {
                "destination_reached": {
                  "value": True
                }
              }
            },
            {
              "term": {
                "path_complete": {
                  "value": True
                }
              }
            },
            {
              "term": {
                "ipv6": {
                  "value": ipv6
                }
              }
            }
          ]
        }
      },
        "aggs": {
            "the_hash": {
              "terms": {
                "field": "route-sha1",
                "order": {"_count" : "desc"},
                "size": 10000
              }
            }
          }
        }
    
    datah = es.search(index='ps_trace', body=queryh)  
    hash_arr = []
    if not datah['aggregations']['the_hash']['buckets']:
        #print("no existing path for ", src, " and ", dest)
        #return 0
        return False
    for x in datah['aggregations']['the_hash']['buckets']:
            hash_arr.append(x['key'])
    
    return(len(hash_arr))
    
    """
    paths = []
    
    for x in hash_arr:
        queryp = {
          "size": 0,
          "query": {
            "bool": {
              "filter": [
                {
                  "term": {
                    "route-sha1": {
                      "value": x
                    }
                  }
                },
                {
                  "term": {
                    "src_host": {
                      "value": src
                    }
                  }
                },
                {
                  "term": {
                    "dest_host": {
                      "value": dest
                    }
                  }
                },
                {
                  "term": {
                    "destination_reached": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "hops_list": {
                  "top_hits": {
                    "size": 1
                  }
                }
              }
            }
        rawp = es.search(index='ps_trace', body=queryp) 
        hops = rawp['aggregations']['hops_list']['hits']['hits'][0]['_source']['hops']
        paths.append(hops)
        
    return paths
    """

def up_test(es, tfrom, tto, ipv6, run):
    #To use this function, uncomment line 77, and comment out the rest of unique_paths after that
    querys = {
       "size": 0,
         "query": {
           "bool": {
             "filter": [
               {
                 "range": {
                   "timestamp": {
                     "lte": tto,
                      "gte": tfrom
                   }
                 }
               },
               {
                 "term": {
                   "destination_reached": {
                     "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "srcs": {
                  "terms": {
                    "field": "src_host",
                    "size": 10000
                  }
                }
              }
            }
    datas = es.search(index='ps_trace', body=querys)
    src_arr = []
    for x in datas['aggregations']['srcs']['buckets']:
        src_arr.append(x['key'])
        
    queryd = {
       "size": 0,
         "query": {
           "bool": {
             "filter": [
               {
                 "range": {
                   "timestamp": {
                     "lte": tto,
                      "gte": tfrom
                   }
                 }
               },
               {
                 "term": {
                   "destination_reached": {
                     "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "dests": {
                  "terms": {
                    "field": "dest_host",
                    "size": 10000
                  }
                }
              }
            }
    datad = es.search(index='ps_trace', body=queryd)
    dest_arr = []
    for x in datad['aggregations']['dests']['buckets']:
        dest_arr.append(x['key'])
    
    n = run
    arr = []
    while (n != 0):
        thing = unique_paths(es, random.choice(src_arr), random.choice(dest_arr), tfrom, tto, ipv6)
        if (thing != False):
            arr.append(thing)
            print(n)
            n -= 1
    
    sd = statistics.stdev(arr)
    avg = statistics.mean(arr)
    
    print("Average number of paths per s/d pair: ", avg)
    print("Standard deviation of paths per s/d pair: ", sd)