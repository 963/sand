import random
import math
import statistics
import queries as qrs

def s_d(es, ipv6, l_date, g_date, mil, num_pair):
    date = random.randint(int(l_date)/1000, int(g_date)/1000)
    date = date*1000
    r_sites = []
    while (num_pair != 0):
        querys = {
          "size": 0,
          "query": {
            "bool": {
              "filter": [
                {
                  "range": {
                    "timestamp": {
                      "lte": date,
                      "gte": int(date) - (mil*3600000)
                    }
                  }
                },
                {
                  "term": {
                    "destination_reached": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "ipv6": {
                      "value": ipv6
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "srcs": {
                  "terms": {
                    "field": "src_host",
                    "size": 500
                  }
                }
              }
            }
        datas = es.search(index='ps_trace', body=querys)
        src_arr = []
        for x in datas['aggregations']['srcs']['buckets']:
            src_arr.append(x['key'])
        if src_arr:
            src = random.choice(src_arr)

            queryd = {
              "size": 0,
                "query": {
                "bool": {
                  "filter": [
                    {
                      "range": {
                        "timestamp": {
                          "lte": date,
                          "gte": int(date) - (mil*3600000)
                        }
                      }
                    },
                    {
                      "term": {
                        "src_host": {
                          "value": src
                        }
                      }
                    },
                    {
                      "term": {
                        "destination_reached": {
                            "value": True
                        }
                      }
                    },
                    {
                        "term": {
                        "path_complete": {
                          "value": True
                        }
                      }
                    },
                    {
                      "term": {
                        "ipv6": {
                          "value": ipv6
                         }
                      }
                    }
                  ]
                }
              },
              "aggs": {
                "dests": {
                  "terms": {
                    "field": "dest_host",
                    "size": 500
                  }
                }
              }
            }
            datad = es.search(index='ps_trace', body=queryd)
            dest_arr = []
            for x in datad['aggregations']['dests']['buckets']:
                dest_arr.append(x['key'])
            r_sites.append(src)
            r_sites.append(random.choice(dest_arr))
            num_pair -= 1
    
    return qrs.common_hops(es, ipv6, date, mil, False, r_sites)

def ch_test(es, ipv6, l_date, g_date, mil, run, num_pair):
    n = run
    arr = []
    while (n != 0):
        thing = s_d(es, ipv6, l_date, g_date, mil, num_pair)
        if (thing != False):
            arr.append(thing)
            print(n)
            n -= 1

    diff = 0
    loop = 0
    avg_b = 0
    avg_a = 0
    arr_b = []
    arr_a = []
    for x in arr:
        if (x[0] > 0):
            diff += 1 #TODO: could do avgs for these too?
        if (x[1] > 0):
            loop += 1
        avg_b += x[2]
        arr_b.append(x[2])
        avg_a += x[3]
        arr_a.append(x[3])

    avg_b = avg_b/run 
    avg_a = avg_a/run
    
    sd_b = statistics.stdev(arr_b)
    sd_a = statistics.stdev(arr_a)
    
    print("Different before and after: ", diff, "/", run)
    print("Has loops: ", loop, "/", run)
    print("Average time found before input: ", avg_b)
    print("Average time found after input: ", avg_a)
    print("Standard deviation for time before input: ", sd_b)
    print("Standard deviation for time after input: ", sd_a)
    





    





